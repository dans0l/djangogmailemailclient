# Integration of Google Email Client in Django

## Quick Guide
Below are the steps on how to get the web app up and running 

1.	Clone it:
```bash
    git clone https://bitbucket.org/dans0l/djangogmailemailclient.git
```
2.	Cd into it:
```bash
    cd djangogmailemailclient
```
3.	Create a venv
```bash
    python3 -m venv venv
```
4.	Activate venv:
```bash
    Mac/Linux: source venv/bin/activate
    Windows: venv\Scripts\activate
```
5.	Install the requirements
```bash
    pip install -r requirements.txt
```
6.	Create DB
```bash
    python manage.py makemigrations
```
7.	Apply DB Changes
```bash
    python manage.py migrate
```
8.	Configure your Google Email Client:
    - Create your Google Account if you don’t have one
    - Generate your Google Account Generate App Passwords  (read above how to do so)
    - Open a json  file labeled "config.json" inside the  “djangogmailemailclient” project folder
    - Enter the credentials in the file
9.	Run the server
```bash
    python manage.py runserver
```
10.	Navigate to your [localhost](http://127.0.0.1:8000) site
11.	Follow the instructions on the home page to start using the site